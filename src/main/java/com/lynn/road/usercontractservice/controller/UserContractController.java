package com.lynn.road.usercontractservice.controller;

import com.lynn.road.usercontractservice.entity.UserContract;
import com.lynn.road.usercontractservice.service.UserContractService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author Simon-the-coder
 * @date 28/11/21 11:14 AM
 */

@RestController
@RequestMapping("/user-contract")
@Slf4j
public class UserContractController {

    @Autowired
    private UserContractService userContractService;

    @GetMapping("/getusercontacts")
    public UserContract getUserContractByUserId(@RequestParam(name="id", required=false) String userId, @RequestParam(required=false) String username) {
        log.info("UserContractController getUserContractByUserId userId " + userId + " username " + username);
        if (userId != null && !userId.isEmpty()) {
            return userContractService.getUserContractByUserId(userId.trim());
        } else if(username != null && !username.isEmpty()) {
            return userContractService.getUserContractByUsername(username.trim());
        }
        return userContractService.getDefaultUserContract();
    }
}
