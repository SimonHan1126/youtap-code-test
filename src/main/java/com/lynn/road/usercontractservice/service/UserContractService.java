package com.lynn.road.usercontractservice.service;

import com.lynn.road.usercontractservice.entity.UserContract;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Simon-the-coder
 * @date 28/11/21 11:15 AM
 */

@Service
@Slf4j
public class UserContractService {

    @Autowired
    private RestTemplate restTemplate;

    private UserContract getUserContractByFilter(Map filterMap) {
        UserContract[] userContracts = restTemplate.getForObject("https://jsonplaceholder.typicode.com/users", UserContract[].class);
        if (userContracts != null && userContracts.length != 0 && filterMap != null) {
            String filterId = (String) filterMap.get("id");
            String filterUsername = (String) filterMap.get("username");
            if ((filterId == null || filterId.isEmpty()) && (filterUsername == null || filterUsername.isEmpty())) {
                return getDefaultUserContract();
            }
            for (UserContract userContract : userContracts) {
                String id = userContract.getId();
                String username = userContract.getUsername();
                if ((filterId !=null && filterId.equals(id)) || (filterUsername!=null && filterUsername.equals(username))) {
                    return userContract;
                }
            }
        }
        return getDefaultUserContract();
    }

    public UserContract getUserContractByUserId(String userId) {
        log.info("UserContractService getUserContractByUserId userId " + userId);
        Map<String, String> filterMap = new HashMap<>();
        filterMap.put("id", userId);
        return getUserContractByFilter(filterMap);
    }

    public UserContract getUserContractByUsername(String username) {
        log.info("UserContractService getUserContractByUsername username " + username);
        Map filterMap = new HashMap();
        filterMap.put("username", username);
        return getUserContractByFilter(filterMap);
    }

    public UserContract getDefaultUserContract() {
        log.info("UserContractService getDefaultUserContract");
        return new UserContract("-1","", "", "");
    }
}
