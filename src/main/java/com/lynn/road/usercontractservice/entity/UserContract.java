package com.lynn.road.usercontractservice.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * @author Simon-the-coder
 * @date 28/11/21 11:15 AM
 */

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserContract {

    @Id
    private String id;
    private String username;
    private String email;
    private String phone;
}
