# Youtap Code Test

This is project is a Spring Boot Microservice. 

It provides an API which accepts GET REST request. 

Request body need to contain a parameter: id or username, id is for user's UserId and username is user's name. 

If this microservice is being run on local, the request url can be 

http://localhost:9001/user-contract/getusercontacts?username=Bret
or 
http://localhost:9001/user-contract/getusercontacts?id=1